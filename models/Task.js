import mongoose from 'mongoose';

// Schema
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
}, { timestamps: true });

// Model
export default mongoose.model('Task', taskSchema);