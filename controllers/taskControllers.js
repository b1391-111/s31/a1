import Task from '../models/Task.js';

export const getTask = async (req, res) => {
    const result = await Task.findById(req.params.id);
    if(result) {
        res.status(200).json(result);
    } else {
        res.status(404).send(`Task not found!`);
    }
}

export const addTask = async (req, res) => {
    const result = await Task.findOne({name: req.body.name});
    if(result === null){
        const newTask = new Task(req.body);
        newTask.save((err, result) => {
            if(err) {
                return res.send(err);
            } else {
                return res.send(`new task: ${result}`);
            }
        });
    } else {
        return res.status(404).send(`Task already exist!`);
    }
}

export const completeTask = async (req, res) => {
    const result = await Task.findById(req.params.id);
    if(result){
        const updatedTask = await Task.findByIdAndUpdate(req.params.id, { status: "complete" }, { new: true });
        return res.status(204).json(updatedTask);
    } else {
        res.send(`Task not found!`);
    }
}
