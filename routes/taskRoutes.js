import express from 'express';
import * as taskController from '../controllers/taskControllers.js';

const router = express.Router();

router.post('/add-task', taskController.addTask);
router.get('/:id', taskController.getTask);
router.put('/:id/complete', taskController.completeTask);

export default router;